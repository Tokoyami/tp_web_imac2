/* eslint-disable */
import React from 'react';

const Hero = (props) => {
  const hName  = props.name;
  const hFullName = props.fullName;
  const hImage = props.images;
  const hAppearance = props.appearance;
  const hWork = props.work;

  return (
    <div className="hero_container">
      <img src={hImage.md} alt={hName} className="hero_img" />
      <h2>{hName}</h2>
      <h3>{hFullName}</h3>
      <div className="hero_appearance">
        <p>
          Race: 
          {hAppearance.race}
        </p>
        <p>
          Gender: 
          {hAppearance.gender}
        </p>
      </div>
      <div className="hero_work">
        <p>
          Occupation: 
          {hWork.occupation}
        </p>
      </div>
    </div>
  );
};

export default Hero;
/* eslint-disable */