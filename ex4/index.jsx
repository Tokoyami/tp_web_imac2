import React, { useEffect, useState } from 'react';
import './ex4.css';
import Hero from './Hero';

const displayHeroes = (heroes) => (
  <>
    <h1>Heroes :</h1>
    <div className="heroes_container">
      {
        heroes.map(
          (hero) => (
            <Hero
              name={hero.name}
              fullName={hero.biography.fullName}
              images={hero.images}
              appearance={hero.appearance}
              work={hero.work}
            />
          ),
        )
      }
    </div>
  </>
);

// eslint-disable-next-line import/prefer-default-export
export const ExerciseFourView = () => {
  // eslint-disable-next-line no-unused-vars
  const [superheroes, setSuperheroes] = useState([]);

  useEffect(() => {
    fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json')
      .then((res) => res.json())
      .then((heroes) => setSuperheroes(heroes));
  });

  return (
    <div>
      {
        superheroes.length > 0 && displayHeroes(superheroes)
      }
    </div>
  );
};
